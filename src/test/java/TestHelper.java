import lombok.Getter;
import restService.DataBase.DataBaseConnector;
import restService.Model.Product;

import java.sql.*;
import java.util.List;

/**
 * Created on 05.02.2018.
 */
public class TestHelper {
    @Getter
    static DataBaseConnector dbConnector = DataBaseConnector.getInstance();

    static Connection connect(){
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(String.format("jdbc:sqlite:C:\\Users\\Koju\\IdeaProjects\\ShoppingListRestService\\sqlite\\db//%s", dbConnector.getDB_NAME()));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    static int getTotalCount(String deviceId, String tableName){
        String sql = String.format("SELECT count(*) AS total FROM %s WHERE deviceId = '%s'", tableName, deviceId);

        int total = 0;
        try (Connection conn = connect();
             Statement query = conn.createStatement();
             ResultSet dataSet = query.executeQuery(sql)){

            while(dataSet.next()){
                total = dataSet.getInt("total");
            }

        }catch (SQLException e ){
            System.out.println(e.getMessage());
        }
        return total;
    }

    static int getTotalBoughtProductsCount(String deviceId, boolean onlyBoughtProducts){
        int bought = onlyBoughtProducts ? 1:0;

        String sql = String.format("SELECT count(*) AS total FROM products WHERE deviceId = '%s' AND isBought = %d", deviceId, bought);

        int total = 0;
        try (Connection conn = connect();
             Statement query = conn.createStatement();
             ResultSet dataSet = query.executeQuery(sql)){

            while(dataSet.next()){
                total = dataSet.getInt("total");
            }

        }catch (SQLException e ){
            System.out.println(e.getMessage());
        }

        return total;
    }

    static boolean getConcreteData(int id, String deviceId){
        String sql = String.format("SELECT * FROM products WHERE id = %d AND deviceId = '%s'", id, deviceId);

        try (Connection conn = connect();
             Statement query = conn.createStatement();
             ResultSet dataSet = query.executeQuery(sql)){

            while(dataSet.next()){
                return true;
            }

        }catch (SQLException e ){
            System.out.println(e.getMessage());
        }
        return false;
    }

    static void deleteProducts(List<Product> productList){
        if(productList != null)
        for (Product product :productList) {
            dbConnector.deleteProduct(product);
        }
    }
}
