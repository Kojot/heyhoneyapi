import org.junit.*;
import restService.DataBase.DataBaseConnector;
import restService.Model.*;

import java.util.Calendar;
import java.util.List;

/**
 * Created on 05.02.2018.
 */
public class DataBaseConnectorTest {
    private static DataBaseConnector dbConnector;
    private Calendar calendar;
    private List<Product> productList;

    @BeforeClass
    public static void setDataBase(){
        dbConnector = TestHelper.getDbConnector();
    }

    @Before
    public void prepareObjects() {
        calendar = Calendar.getInstance();
        productList = null;
    }

    @After
    public void clear(){
        TestHelper.deleteProducts(productList);
    }

    @Test
    public void shouldInsertApplicationInfoIntoDB() {
        dbConnector.insertApplicationInfo(new ApplicationInfo(DeviceInfo.builder().deviceId("123").build(), User.builder().userName("Janusz").build()));

        ApplicationInfo info = dbConnector.getApplicationInfo("123");

        Assert.assertEquals("Janusz", info.getUser().getUserName());
        Assert.assertEquals("123", info.getDeviceInfo().getDeviceId());
    }

    @Test
    public void shouldExistsOnlyOneRowWithUniqueId(){
        ApplicationInfo info = new ApplicationInfo(DeviceInfo.builder().deviceId("345").build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(info);
        dbConnector.insertApplicationInfo(info);

        int total = TestHelper.getTotalCount("345", "applicationInfo");

        Assert.assertEquals(1, total);
    }

    @Test
    public void shouldAddProductsToUniqueUser(){
        String user1Id = "111";
        String user2Id = "345";

        ApplicationInfo user1 = new ApplicationInfo(DeviceInfo.builder().deviceId(user1Id).build(), User.builder().userName("Tomek").build());
        ApplicationInfo user2 = new ApplicationInfo(DeviceInfo.builder().deviceId(user2Id).build(), User.builder().userName("Tomek").build());

        dbConnector.insertApplicationInfo(user1);
        dbConnector.insertApplicationInfo(user2);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(user1Id).isBought(0).timestamp(calendar.getTimeInMillis()).build());
        dbConnector.insertProduct(Product.builder().name("Milk").quantity("2").unit("l").deviceId(user1Id).timestamp(calendar.getTimeInMillis()).isBought(0).build());
        dbConnector.insertProduct(Product.builder().name("Whiskey").quantity("2").unit("l").deviceId(user2Id).timestamp(calendar.getTimeInMillis()).isBought(0).build());
        dbConnector.insertProduct(Product.builder().name("Cola").quantity("2").unit("l").deviceId(user2Id).timestamp(calendar.getTimeInMillis()).isBought(0).build());

        productList = dbConnector.getActualProductList(user1Id);

        Assert.assertEquals(2, productList.size());
    }

    @Test
    public void shouldNotAddProductWhenThereIsNoUser() {
        String userId = "3948576212";

        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());

        int total = TestHelper.getTotalCount(userId, "products");

        Assert.assertEquals(0, total);
    }

    @Test
    public void shouldNotReturnListForUserThatDoesNotExists() {
        productList = dbConnector.getActualProductList("000");

        Assert.assertEquals(0, productList.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForNoNameProduct() {
        String userId = "111";
        ApplicationInfo user1 = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(user1);

        dbConnector.insertProduct(Product.builder().name("").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForZeroQuantity() {
        String userId = "111";
        ApplicationInfo user1 = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(user1);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("0").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForNegativeQuantity() {
        String userId = "111";
        ApplicationInfo user1 = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(user1);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("-2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForEmptyQuantity() {
        String userId = "111";
        ApplicationInfo user1 = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(user1);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForProductWithNoUnit() {
        String userId = "111";
        ApplicationInfo user1 = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(user1);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("5").unit("").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForProductWithoutOwner(){
        String userId = "111";
        ApplicationInfo user1 = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(user1);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("5").unit("l").deviceId("").isBought(0).timestamp(calendar.getTimeInMillis()).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForEmptyProduct(){
        String userId = "111";
        ApplicationInfo user1 = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(user1);

        dbConnector.insertProduct(Product.builder().name("").quantity("").unit("").deviceId("").isBought(0).timestamp(0).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForFakeProductId(){
        String userId = "111";
        ApplicationInfo user1 = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(user1);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).id(123).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForIllegalBoughtState(){
        String userId = "111";
        ApplicationInfo user1 = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(user1);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(99).timestamp(calendar.getTimeInMillis()).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForIllegalTimeStamp() {
        String userId = "111";
        ApplicationInfo user1 = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(user1);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(1514764700).build());
    }

    @Test
    public void shouldAddFloatQuantityProduct() {
        String userId = "111";
        ApplicationInfo user1 = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(user1);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("0.5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());

        productList = dbConnector.getActualProductList(userId);
        Product product = productList.get(0);

        Assert.assertEquals("0.5", product.getQuantity());
    }

    @Test
    public void shouldGetOnlyBoughtProducts(){
        String userId = "1029";
        ApplicationInfo info = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(info);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(1).timestamp(calendar.getTimeInMillis()).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(1).timestamp(calendar.getTimeInMillis()).build());

        productList = dbConnector.getSuggestedProductList(userId, calendar.getTimeInMillis());

        Assert.assertEquals(2, productList.size());
    }

    @Test
    public void shouldGetOnlyProductsToBuy(){
        String userId = "1029999";
        ApplicationInfo info = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(info);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());

        productList = dbConnector.getActualProductList(userId);

        Assert.assertEquals(4, productList.size());
    }

    @Test
    public void shouldDeleteConcreteProduct() {
        String userId = "000999000";
        ApplicationInfo info = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(info);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
        dbConnector.insertProduct(Product.builder().name("ProduktDoUsunięcia").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
        productList = dbConnector.getActualProductList(userId);

        Product productWithId = productList.get(1);
        int id = productWithId.getId();

        dbConnector.deleteProduct(productWithId);

        productList = dbConnector.getActualProductList(userId);

        for (Product productOnList : productList) {
            Assert.assertFalse(productOnList.getId() == id);
        }
    }

    @Test
    public void shouldUpdateSameProduct() {
        String userId = "1029384756";
        ApplicationInfo info = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(info);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build());
        productList = dbConnector.getActualProductList(userId);

        Product productWithId = productList.get(0);
        int visibilityBefore = productWithId.getIsBought();
        int id = productWithId.getId();

        dbConnector.updateProduct(productWithId);

        productList = dbConnector.getSuggestedProductList(userId, calendar.getTimeInMillis());

        int visibilityAfter = productList.get(0).getIsBought();

        Assert.assertTrue(productList.get(0).getId() == id);
        Assert.assertTrue(visibilityBefore != visibilityAfter);
    }

    @Test
    public void shouldAddProductValuesToProperColumns(){
        String deviceId = "0", productsharedUserId = "6767";

        String productName = "Water", productQuantity = "2", productUnit = "l";
        int productIsBought = 0;
        long productTimestamp = 1700000000;

        ApplicationInfo info = new ApplicationInfo(DeviceInfo.builder().deviceId(deviceId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(info);

        dbConnector.insertProduct(Product.builder()
                .name(productName)
                .quantity(productQuantity)
                .unit(productUnit)
                .deviceId(deviceId)
                .isBought(productIsBought)
                .timestamp(productTimestamp)
                .sharedUserId(productsharedUserId)
                .build());

        productList = dbConnector.getActualProductList(deviceId);

        Product productFromDb = productList.get(0);

        Assert.assertTrue(productName.equals(productFromDb.getName()));
        Assert.assertTrue(productQuantity.equals(productFromDb.getQuantity()));
        Assert.assertTrue(productUnit.equals(productFromDb.getUnit()));
        Assert.assertTrue(deviceId.equals(productFromDb.getDeviceId()));
        Assert.assertTrue(productIsBought == productFromDb.getIsBought());
        Assert.assertTrue(productTimestamp == productFromDb.getTimestamp());
        Assert.assertTrue(productsharedUserId.equals(productFromDb.getSharedUserId()));

        dbConnector.deleteProduct(productFromDb);
    }

    @Test
    public void shouldReturnSuggestedProductList(){
        String userId = "11223344";
        ApplicationInfo info = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(info);

        Calendar calendar = Calendar.getInstance();
        long timePresent = calendar.getTimeInMillis();
        calendar.set(Calendar.DATE, -14);
        long timePast = calendar.getTimeInMillis();

        calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, -12);
        long timeAsking = calendar.getTimeInMillis();

        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(timePresent).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(timePresent).build());
        dbConnector.insertProduct(Product.builder().name("StaryKupionyProdukt1").quantity("2").unit("l").deviceId(userId).isBought(1).timestamp(timePast).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(timePresent).build());
        dbConnector.insertProduct(Product.builder().name("StaryKupionyProdukt2").quantity("2").unit("l").deviceId(userId).isBought(1).timestamp(timePast).build());

        productList = dbConnector.getSuggestedProductList(userId, timeAsking);

        Assert.assertEquals(2, productList.size());
    }

    @Test
    public void shouldReturnEmptyListForSuggestedListWhenTimeDoNotElapse() {
        String userId = "44332211";
        ApplicationInfo info = new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build());
        dbConnector.insertApplicationInfo(info);

        Calendar calendar = Calendar.getInstance();
        long timePresent = calendar.getTimeInMillis();
        calendar.set(Calendar.DATE, -14);
        long timePast = calendar.getTimeInMillis();

        calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, -18);
        long askingTime = calendar.getTimeInMillis();

        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(timePresent).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(timePresent).build());
        dbConnector.insertProduct(Product.builder().name("ProduktKupionyNieDoSugestii").quantity("2").unit("l").deviceId(userId).isBought(1).timestamp(timePast).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(userId).isBought(0).timestamp(timePresent).build());
        dbConnector.insertProduct(Product.builder().name("ProduktKupionyNieDoSugestii").quantity("2").unit("l").deviceId(userId).isBought(1).timestamp(timePast).build());

        productList = dbConnector.getSuggestedProductList(userId, askingTime);

        Assert.assertEquals(0, productList.size());
    }

    @Test
    public void shouldReturnSharedProductList() {
        String user1Id = "555550";
        String user2Id = "666660";

        ApplicationInfo info1 = new ApplicationInfo(DeviceInfo.builder().deviceId(user1Id).build(), User.builder().userName("Tomek").build());
        ApplicationInfo info2 = new ApplicationInfo(DeviceInfo.builder().deviceId(user2Id).build(), User.builder().userName("Tomek").build());

        dbConnector.insertApplicationInfo(info1);
        dbConnector.insertApplicationInfo(info2);

        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(user1Id).isBought(1).timestamp(calendar.getTimeInMillis()).sharedUserId(user2Id).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(user1Id).isBought(1).timestamp(calendar.getTimeInMillis()).sharedUserId(user2Id).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(user2Id).isBought(1).timestamp(calendar.getTimeInMillis()).sharedUserId(user1Id).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(user2Id).isBought(1).timestamp(calendar.getTimeInMillis()).sharedUserId(user1Id).build());
        dbConnector.insertProduct(Product.builder().name("Water").quantity("2").unit("l").deviceId(user1Id).isBought(1).timestamp(calendar.getTimeInMillis()).build());

        productList = dbConnector.getSharedProductList(user2Id);

        Assert.assertEquals(2, productList.size());
    }

//    @Test
//    public void temp() {
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.DATE, -14);
//
//        long timeForSure = cal.getTimeInMillis();
//        cal = Calendar.getInstance();
//        cal.set(Calendar.DATE, -1);
//        long timeNot = cal.getTimeInMillis();
//        ApplicationInfo info1 = new ApplicationInfo(DeviceInfo.builder().deviceId("893").build(), User.builder().userName("Tomek").build());
//        dbConnector.insertApplicationInfo(info1);
//        dbConnector.insertProduct(Product.builder().name("Produkt Udostępniony przez innego").quantity("2").unit("l").deviceId("893").isBought(0).timestamp(calendar.getTimeInMillis()).sharedUserId("1").build());

//        dbConnector.insertProduct(Product.builder().name("Kolejka").quantity("2").unit("szt.").deviceId("000000000000000").isBought(0).timestamp(timeForSure).build());
//        dbConnector.insertProduct(Product.builder().name("Wiertło").quantity("8").unit("szt.").deviceId("000000000000000").isBought(0).timestamp(timeForSure).build());
//        dbConnector.insertProduct(Product.builder().name("Soczek").quantity("15").unit("l").deviceId("000000000000000").isBought(0).timestamp(timeForSure).build());
//        dbConnector.insertProduct(Product.builder().name("!TEGO TU NIE MA!").quantity("15").unit("l").deviceId("000000000000000").isBought(1).timestamp(timeNot).build());
//        dbConnector.insertProduct(Product.builder().name("!TEGO TEż NIE MA!").quantity("15").unit("l").deviceId("000000000000000").isBought(1).timestamp(timeNot).build());
//    }
}
