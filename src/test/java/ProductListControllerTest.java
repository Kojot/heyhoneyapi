import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import restService.Controllers.ApplicationController;
import restService.Controllers.ProductListController;
import restService.Model.*;

import java.util.Calendar;

/**
 * Created on 05.02.2018.
 */
public class ProductListControllerTest {
    ProductListController productListController;
    ApplicationController applicationController;
    Calendar calendar;

    @Before
    public void setUp() {
        applicationController = new ApplicationController();
        productListController = new ProductListController();
        calendar = Calendar.getInstance();
    }

    @Test
    public void shouldReturnCorrectProductList() {
        String userId = "999";
        applicationController.sendApplicationInfo(new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build()));

        productListController.updateProductList(Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build(),calendar.getTimeInMillis());
        productListController.updateProductList(Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build(),calendar.getTimeInMillis());
        productListController.updateProductList(Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build(),calendar.getTimeInMillis());

        ProductList productList = productListController.getProductList(userId, calendar.getTimeInMillis());

        int total = TestHelper.getTotalCount(userId, "products");
        Assert.assertEquals(total, productList.getProductList().size());
        Assert.assertTrue(productList.getProductList().size() != 0);
    }

    @Test
    public void shouldDeleteProduct(){
        String userId = "123456";
        applicationController.sendApplicationInfo(new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build()));

        Product product = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build();
        Product product1 = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build();
        Product product2 = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build();
        productListController.updateProductList(product, calendar.getTimeInMillis());
        productListController.updateProductList(product1, calendar.getTimeInMillis());
        ProductList updatedProductList = productListController.updateProductList(product2, calendar.getTimeInMillis());

        Product productWithId = updatedProductList.getProductList().get(0);
        int id = productWithId.getId();

        productListController.deleteProduct(productWithId);

        Assert.assertFalse(TestHelper.getConcreteData(id, userId));
    }

    @Test
    public void shouldReturnOnlyProductsToBuy(){
        String userId = "77777777";
        applicationController.sendApplicationInfo(new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build()));

        Product product = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build();
        Product product1 = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build();
        Product product2 = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build();
        productListController.updateProductList(product, calendar.getTimeInMillis());
        productListController.updateProductList(product1, calendar.getTimeInMillis());
        productListController.updateProductList(product2, calendar.getTimeInMillis());

        ProductList productList = productListController.getProductList(userId, calendar.getTimeInMillis());

        Assert.assertEquals(3, productList.getProductList().size());

        productListController.deleteProduct(productList.getProductList().get(0));
        productListController.deleteProduct(productList.getProductList().get(1));
        productListController.deleteProduct(productList.getProductList().get(2));
    }

    @Test
    public void shouldReturnOnlySuggestedProducts(){
        String userId = "88888888";
        applicationController.sendApplicationInfo(new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build()));

        Product product = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build();
        Product product1 = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build();
        Product product2 = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build();
        productListController.updateProductList(product, calendar.getTimeInMillis());
        productListController.updateProductList(product1, calendar.getTimeInMillis());
        ProductList updatedProductList = productListController.updateProductList(product2, calendar.getTimeInMillis());

        productListController.updateProduct(updatedProductList.getProductList().get(0));
        productListController.updateProduct(updatedProductList.getProductList().get(1));
        productListController.updateProduct(updatedProductList.getProductList().get(2));

        ProductList productList = productListController.getProductList(userId, calendar.getTimeInMillis());

        Assert.assertEquals(3, productList.getProductList().size());
        Assert.assertTrue(productList.getProductList().size() != 0);

        productListController.deleteProduct(updatedProductList.getProductList().get(0));
        productListController.deleteProduct(updatedProductList.getProductList().get(1));
        productListController.deleteProduct(updatedProductList.getProductList().get(2));
    }

    @Test
    public void shouldReturnProductListWithSuggestedProducts() {
        String userId = "777888777";
        applicationController.sendApplicationInfo(new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build()));

        Calendar calendar = Calendar.getInstance();
        long timePresent = calendar.getTimeInMillis();
        calendar.set(Calendar.DATE, -14);
        long timePast = calendar.getTimeInMillis();

        calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, -7);
        long timeAsking = calendar.getTimeInMillis();

        System.out.println(timePast < timeAsking);

        Product product = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(timePresent).build();
        Product product1 = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(timePresent).build();
        Product product2 = Product.builder().name("Stary Kupiony Produkt").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(timePast).build();


        productListController.updateProductList(product, calendar.getTimeInMillis());
        productListController.updateProductList(product1, calendar.getTimeInMillis());
        ProductList updatedProductList = productListController.updateProductList(product2, calendar.getTimeInMillis());

        Product temp = updatedProductList.getProductList().get(2);
        productListController.updateProduct(temp);

        ProductList productList = productListController.getProductList(userId, timeAsking);

        Assert.assertEquals(3, productList.getProductList().size());

        productListController.deleteProduct(updatedProductList.getProductList().get(0));
        productListController.deleteProduct(updatedProductList.getProductList().get(1));
        productListController.deleteProduct(updatedProductList.getProductList().get(2));
    }

    @Test
    public void shouldReturnSharedProductList() {
        String userId = "10000001";
        String sharedUserId = "1000001";
        applicationController.sendApplicationInfo(new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build()));

        Product product = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).sharedUserId(sharedUserId).build();
        Product product1 = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).sharedUserId(sharedUserId).build();
        Product product2 = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).sharedUserId(sharedUserId).build();

        productListController.updateSharedProductList(product);
        productListController.updateSharedProductList(product1);
        ProductList sharedProductList =  productListController.updateSharedProductList(product2);

        Assert.assertEquals(3, sharedProductList.getProductList().size());

        productListController.deleteProduct(sharedProductList.getProductList().get(0));
        productListController.deleteProduct(sharedProductList.getProductList().get(1));
        productListController.deleteProduct(sharedProductList.getProductList().get(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenNosharedUserIdOnSharedProduct() {
        String userId = "00000001";
        String sharedUserId = "1000000";
        applicationController.sendApplicationInfo(new ApplicationInfo(DeviceInfo.builder().deviceId(userId).build(), User.builder().userName("Tomek").build()));

        Product product = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).sharedUserId(sharedUserId).build();
        Product product1 = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).sharedUserId(sharedUserId).build();
        Product product2 = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).sharedUserId(sharedUserId).build();
        Product product3 = Product.builder().name("Water").quantity("5").unit("l").deviceId(userId).isBought(0).timestamp(calendar.getTimeInMillis()).build();

        productListController.updateSharedProductList(product);
        productListController.updateSharedProductList(product1);
        productListController.updateSharedProductList(product2);
        ProductList sharedProductList = productListController.updateSharedProductList(product3);
    }
}
