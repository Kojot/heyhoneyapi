package restService.Controllers;

import org.springframework.web.bind.annotation.*;
import restService.Model.Product;
import restService.Model.ProductList;

import java.time.Instant;

/**
 * Created on 08.12.2017.
 */
@RestController
public class ProductListController extends BaseController{
    private ProductList productList;

    public ProductListController() {
        productList = new ProductList(connector);
    }

    @RequestMapping(value = "/getProductList/{deviceId}/{time}", method = RequestMethod.GET)
    public ProductList getProductList(@PathVariable String deviceId,@PathVariable long time){
        System.out.println(String.format("API request: >>> /getProductList <<<>>> %s", Instant.now().toString()));
        productList.getProductList(deviceId, time);
        return productList;
    }

    @RequestMapping(value = "/getSharedProductList/{sharedUserId}", method = RequestMethod.GET)
    public ProductList getSharedProductList(@PathVariable String sharedUserId){
        System.out.println(String.format("API request: >>> /getSharedProductList <<<>>> %s", Instant.now().toString()));
        productList.getSharedProductList(sharedUserId);
        return productList;
    }

    @RequestMapping(value = "/updateProductList/{time}", method = RequestMethod.POST)
    @ResponseBody
    public ProductList updateProductList(@RequestBody Product product, @PathVariable long time){
        System.out.println(String.format("API request: >>> /updateProductList <<<>>> %s", Instant.now().toString()));
        productList.addProduct(product, time);
        return productList;
    }

    @RequestMapping(value = "/updateSharedProductList", method = RequestMethod.POST)
    @ResponseBody
    public ProductList updateSharedProductList(@RequestBody Product product) {
        System.out.println(String.format("API request: >>> /updateSharedProductList <<<>>> %s", Instant.now().toString()));
        productList.addProduct(product);
        return productList;
    }

    @RequestMapping(value = "/updateProduct", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public String updateProduct(@RequestBody Product product){
        System.out.println(String.format("API request: >>> /updateProduct <<<>>> %s", Instant.now().toString()));
        productList.updateProduct(product);
        return "product updated";
    }

    @RequestMapping(value = "/deleteProduct", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public String deleteProduct(@RequestBody Product product){
        System.out.println(String.format("API request: >>> /deleteProduct <<<>>> %s", Instant.now().toString()));
        productList.deleteProduct(product);
        return "product deleted";
    }
}
