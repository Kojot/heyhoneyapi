package restService.Controllers;

import com.google.gson.Gson;
import org.springframework.web.bind.annotation.*;
import restService.Model.ApplicationInfo;
import java.time.Instant;

/**
 * Created on 17.01.2018.
 */
@RestController
public class ApplicationController extends BaseController {
    private ApplicationInfo applicationInfo;

    public ApplicationController() {
        applicationInfo = new ApplicationInfo(connector);
    }

    @RequestMapping(value = "/getApplicationInfo/{deviceId}", method = RequestMethod.GET)
    public ApplicationInfo getApplicationInfo(@PathVariable("deviceId") String deviceId){
        System.out.println(String.format("API request : >>> /getApplicationInfo <<<>>> %s", Instant.now().toString()));
        return applicationInfo.getApplicationInfo(deviceId);
    }

    @RequestMapping(value = "/sendApplicationInfo", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public String sendApplicationInfo(@RequestBody ApplicationInfo info){
        System.out.println(String.format("API request : >>> /sendApplicationInfo <<<>>> %s", Instant.now().toString()));
        applicationInfo.addApplicationInfo(info);
        return "application info added";
    }
}
