package restService.Model;

import lombok.Getter;
import lombok.Setter;
import restService.DataBase.DataBaseConnector;

/**
 * Created on 17.01.2018.
 */

@Getter
@Setter
public class ApplicationInfo {
    private DeviceInfo deviceInfo;
    private User user;
    private DataBaseConnector dbConnector;

    public ApplicationInfo(DataBaseConnector dbConnector) {
        this.dbConnector = dbConnector;
    }

    public ApplicationInfo(DeviceInfo deviceInfo, User user) {
        this.deviceInfo = deviceInfo;
        this.user = user;
    }

    public void addApplicationInfo(ApplicationInfo info) {
        dbConnector.insertApplicationInfo(info);
    }

    public ApplicationInfo getApplicationInfo(String deviceId){
        return dbConnector.getApplicationInfo(deviceId);
    }

    public ApplicationInfo() {
    }
}
