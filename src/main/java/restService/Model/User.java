package restService.Model;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import restService.DataBase.DataBaseConnector;

/**
 * Created on 11.01.2018.
 */
@Getter
@Builder
public class User {

    private String userName;
    private int userId;
}
