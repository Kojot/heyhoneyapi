package restService.Model;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

/**
 * Created on 17.01.2018.
 */
@Getter
@Builder
public class DeviceInfo {
    String deviceId;
}
