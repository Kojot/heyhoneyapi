package restService.Model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Created on 08.12.2017.
 */
@Getter
@Builder
public class Product {
    private int id;
    private String name;
    private String quantity;
    private String unit;
    private String deviceId;
    private int isBought;
    private long timestamp;
    private String sharedUserId;

}
