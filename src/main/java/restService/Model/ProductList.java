package restService.Model;

import lombok.Getter;
import lombok.Setter;
import restService.DataBase.DataBaseConnector;

import java.util.ArrayList;
import java.util.List;


public class ProductList {
    @Getter
    @Setter
    private List<Product> productList;
    private DataBaseConnector dbConnector;

    public ProductList(DataBaseConnector connector) {
        dbConnector = connector;
    }

    public void addProduct(Product product, long time){
        dbConnector.insertProduct(product);
        getProductList(product.getDeviceId(), time);
    }

    public void addProduct(Product product) {
        dbConnector.insertProduct(product);
        getSharedProductList(product.getSharedUserId());
    }

    public void getProductList(String deviceId, long time) {
        List<Product> actualProductList = dbConnector.getActualProductList(deviceId);
        List<Product> suggestedProductList = dbConnector.getSuggestedProductList(deviceId, time);

        productList = new ArrayList<>();
        productList.addAll(actualProductList);
        productList.addAll(suggestedProductList);
    }

    public void deleteProduct(Product product) {
        dbConnector.deleteProduct(product);
    }

    public void updateProduct(Product product) {
        dbConnector.updateProduct(product);
    }

    public void getSharedProductList(String sharedUserId) {
        productList = dbConnector.getSharedProductList(sharedUserId);
    }
}
