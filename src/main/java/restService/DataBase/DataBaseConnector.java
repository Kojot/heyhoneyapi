package restService.DataBase;

import lombok.Getter;
import lombok.Setter;
import restService.Model.ApplicationInfo;
import restService.Model.DeviceInfo;
import restService.Model.Product;
import restService.Model.User;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

/**
 * Created on 12/19/2017.
 */
public class DataBaseConnector {
    private static DataBaseConnector instance;

    @Getter
    @Setter
    private String DB_NAME = "DEV.db";
    private String URL;//
    private final String PATH = format("sqlite//db//%s", DB_NAME);

    private DataBaseConnector(){
        createDataBase();
    }

    public static DataBaseConnector getInstance(){
        if (instance == null) {
            instance = new DataBaseConnector();
        }
        return instance;
    }

    private void createDataBase() {
        File targetFile = new File(PATH);
        String absolutePath = targetFile.getAbsolutePath();
        File parent = targetFile.getParentFile();
        if (!parent.exists() && !parent.mkdirs()) {
            throw new IllegalStateException("Couldn't create dir: " + parent);
        }

        URL = format("jdbc:sqlite:%s", absolutePath);
        createProductListTable(URL);
        createApplicationInfoTable(URL);
    }

    private void createProductListTable(String url){
        String sql = "CREATE TABLE IF NOT EXISTS products (\n"
                + " id integer PRIMARY KEY,\n"
                + " name text NOT NULL,\n"
                + " quantity text, \n"
                + " unit text, \n"
                + " deviceId text NOT NULL, \n"
                + " isBought integer NOT NULL, \n"
                + " timestamp integer NOT NULL, \n"
                + " sharedUserId text\n);";

        try (Connection connection = DriverManager.getConnection(url);
             Statement stmt = connection.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private void createApplicationInfoTable(String url){
        String sql = "CREATE TABLE IF NOT EXISTS applicationInfo (\n"
                + " id integer PRIMARY KEY, \n"
                + " deviceId text NOT NULL, \n"
                + " userName text NOT NULL\n);";

        try(Connection connection =  DriverManager.getConnection(url);
            Statement stmt = connection.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void insertProduct(Product product) {
        String sql = "INSERT INTO products(name,quantity,unit,deviceId,isBought,timestamp,sharedUserId) VALUES (?,?,?,?,?,?,?)";

        if(validateProduct(product)){
            if (checkIfUserExists(product.getDeviceId()) == 1) {
                try (Connection connection = connect();
                     PreparedStatement query = connection.prepareStatement(sql)) {
                    query.setString(1, product.getName());
                    query.setString(2, product.getQuantity());
                    query.setString(3, product.getUnit());
                    query.setString(4, product.getDeviceId());
                    query.setInt(5, product.getIsBought());
                    query.setLong(6,product.getTimestamp());
                    query.setString(7,product.getSharedUserId());
                    query.executeUpdate();
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
            } else {
                System.out.println("There in no such user");
            }
        }
    }

    public void updateProduct(Product product){
        String sql = String.format("UPDATE products SET isBought = 1 WHERE id = %d AND deviceId = '%s'", product.getId(), product.getDeviceId());

        try (Connection conn = connect();
             Statement query  = conn.createStatement()){

            query.execute(sql);

        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    private boolean validateProduct(Product product){
        final long TIMESTAMP = 1514764800; //2018-01-01 00:00:00.0
        if(product.getName().equals("")){
            throw new IllegalArgumentException("Product with no name");
        }
        else if(product.getQuantity().equals("") || Double.parseDouble(product.getQuantity()) <= 0){
            throw new IllegalArgumentException("Product quantity cannot be negative or zero");
        }
        else if(product.getUnit().equals("")){
            throw new IllegalArgumentException("There is no product unit");
        }
        else if(product.getDeviceId().equals("")){
            throw new IllegalArgumentException("Product has no owner");
        }
        else if(product.getIsBought() != 0 && product.getIsBought() != 1){
            throw new IllegalArgumentException("Visibility state invalid");
        }
        else if(product.getTimestamp() <= TIMESTAMP){
            throw new IllegalArgumentException("Illegal Timestamp");
        }
        else if(product.getId() != 0){
            throw new IllegalArgumentException("Product id compromised!");
        }

        return true;
    }

    public void deleteProduct(Product product) {
        String sql = String.format("DELETE FROM products WHERE id = %d AND deviceId = '%s'", product.getId(), product.getDeviceId());

        try (Connection conn = connect();
             Statement query  = conn.createStatement()){

            query.execute(sql);

        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public List<Product> getActualProductList(String deviceId) {
        List<Product> productList = new ArrayList<>();

        String sql = String.format("SELECT id, name, quantity, unit, deviceId, isBought, timestamp, sharedUserId FROM products WHERE deviceId = '%s' AND isBought = 0", deviceId);

        try (Connection conn = connect();
             Statement query  = conn.createStatement();
             ResultSet dataSet = query.executeQuery(sql)){

            while (dataSet.next()){
                productList.add(
                        Product.builder()
                        .name(dataSet.getString("name"))
                        .quantity(dataSet.getString("quantity"))
                        .unit(dataSet.getString("unit"))
                        .deviceId(dataSet.getString("deviceId"))
                        .isBought(dataSet.getInt("isBought"))
                        .id(dataSet.getInt("id"))
                        .timestamp(dataSet.getLong("timestamp"))
                        .sharedUserId(dataSet.getString("sharedUserId"))
                        .build());
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return productList;
    }

    public List<Product> getSuggestedProductList(String deviceId, long time){
        List<Product> productList = new ArrayList<>();

        String sql = String.format("SELECT id, name, quantity, unit, deviceId, isBought, timestamp, sharedUserId FROM products WHERE deviceId = '%s' AND isBought = 1 AND timestamp <= %d", deviceId, time);

        try (Connection conn = connect();
             Statement query  = conn.createStatement();
             ResultSet dataSet = query.executeQuery(sql)){

            while (dataSet.next()){
                productList.add(
                        Product.builder()
                                .name(dataSet.getString("name"))
                                .quantity(dataSet.getString("quantity"))
                                .unit(dataSet.getString("unit"))
                                .deviceId(dataSet.getString("deviceId"))
                                .isBought(dataSet.getInt("isBought"))
                                .id(dataSet.getInt("id"))
                                .timestamp(dataSet.getLong("timestamp"))
                                .sharedUserId(dataSet.getString("sharedUserId"))
                                .build());
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return productList;
    }

    public List<Product> getSharedProductList(String sharedUserId) {
        List<Product> productList = new ArrayList<>();

        String sql = String.format("SELECT id, name, quantity, unit, deviceId, isBought, timestamp, sharedUserId FROM products WHERE sharedUserId = '%s'", sharedUserId);

        if(sharedUserId == null || sharedUserId.equals("")){
            throw new IllegalArgumentException("No sharedUserId on shared product");
        }else {
            try (Connection conn = connect();
                 Statement query = conn.createStatement();
                 ResultSet dataSet = query.executeQuery(sql)) {

                while (dataSet.next()) {
                    productList.add(
                            Product.builder()
                                    .name(dataSet.getString("name"))
                                    .quantity(dataSet.getString("quantity"))
                                    .unit(dataSet.getString("unit"))
                                    .deviceId(dataSet.getString("deviceId"))
                                    .isBought(dataSet.getInt("isBought"))
                                    .id(dataSet.getInt("id"))
                                    .timestamp(dataSet.getLong("timestamp"))
                                    .sharedUserId(dataSet.getString("sharedUserId"))
                                    .build());
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            return productList;
        }
    }

    public void insertApplicationInfo (ApplicationInfo info) {
        String sql = "INSERT INTO applicationInfo(deviceId,userName) VALUES (?,?)";

        if(checkIfUserExists(info.getDeviceInfo().getDeviceId()) < 1){
            try(Connection connection = connect();
                PreparedStatement query = connection.prepareStatement(sql)){
                query.setString(1, info.getDeviceInfo().getDeviceId());
                query.setString(2, info.getUser().getUserName());
                query.executeUpdate();
            }catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }else{
            System.out.println("Device ID already exists");
        }
    }

    public ApplicationInfo getApplicationInfo(String deviceId){
        ApplicationInfo applicationInfo = null;
        String sql = String.format(
                "SELECT id, deviceId, userName " +
                "FROM applicationInfo " +
                "WHERE deviceId = '%s'", deviceId);

        try(Connection conn = connect();
            Statement query = conn.createStatement();
            ResultSet dataSet = query.executeQuery(sql)){

            while(dataSet.next()){
                applicationInfo = new ApplicationInfo(
                        DeviceInfo.builder()
                                .deviceId(dataSet.getString("deviceId"))
                                .build(),
                        User.builder()
                                .userName(dataSet.getString("userName"))
                                .userId(dataSet.getInt("id"))
                                .build()
                );
            }
        } catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return applicationInfo;
    }

    private Connection connect(){
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(URL);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    private int checkIfUserExists(String deviceId){
        String sql = String.format("SELECT count(*) AS total FROM applicationInfo WHERE deviceId = '%s'",deviceId);

        int total = 0;
        try (Connection conn = connect();
             Statement query = conn.createStatement();
             ResultSet dataSet = query.executeQuery(sql)){

            while(dataSet.next()){
                total = dataSet.getInt("total");
            }

        }catch (SQLException e ){
            System.out.println(e.getMessage());
        }

        return total;
    }


}
