package restService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import restService.Controllers.BaseController;
import restService.DataBase.DataBaseConnector;

import java.util.List;

/**
 * Created on 03.11.2017.
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        BaseController controller = new BaseController();
        SpringApplication.run(Application.class, args);
    }
}
